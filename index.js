//console.log("Hello, World");

/*
	-Create a variable number that will store the value of the number provided by the user via the prompt.
	-Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
	-Create a condition that if the current value is less than or equal to 50, stop the loop.
	-Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
	-Create another condition that if the current value is divisible by 5, print the number.
	
*/



console.log("------------------Part 1------------------");
let number = Number(prompt("Enter a Number:"));
console.log("The number you provided is " + number + ".");
for(let count = number; count >= 0; count--){
	
	if(count <= 50){
		console.log("The current value is at " + count + ". Terminating the loop.");
		break;
	}

	if(count % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number");
		continue;
	}

	if(count % 5 === 0){
		console.log(count);
	}
}


/*
	-Create a variable that will contain the string supercalifragilisticexpialidocious.
	-Create another variable that will store the consonants from the string.
	-Create another for Loop that will iterate through the individual letters of the string based on it’s length.
	-Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
	-Create an else statement that will add the letter to the second variable.
*/

console.log("------------------Part 2------------------");
let word = "supercalifragilisticexpialidocious";
let consonants = "";

for(let index = 0; index < word.length; index++){
	if(word[index].toLowerCase() === 'a' || word[index].toLowerCase() === 'e' || word[index].toLowerCase() === 'i' || word[index].toLowerCase() === 'o' || word[index].toLowerCase() === 'u'){
		continue;
	}else{
		consonants+=word[index];
	}
}

console.log(word);
console.log(consonants);